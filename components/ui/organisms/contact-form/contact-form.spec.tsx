import React from "react"
import { render } from "@testing-library/react"
import { expect } from "chai"

import { ContactForm } from "./contact-form"
describe("ui/organisms/contact-form", () => {
  it("should render the default title", () => {
    const { getByText } = render(<ContactForm />)
    const rendered = getByText("Contact Us")

    expect(rendered).to.exist
  })

  it("should render a custom title", () => {
    const { getByText } = render(<ContactForm title="Hello, World!" />)
    const rendered = getByText("Hello, World!")

    expect(rendered).to.exist
  })
})
