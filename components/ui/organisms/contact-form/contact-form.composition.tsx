import React from "react"
import { TopLayout } from "@waweb/dimensions.ui.layout.top-layout"
import { ContactForm } from "./contact-form"

export const DefaultContactForm = () => {
  return (
    <TopLayout>
      <ContactForm />
    </TopLayout>
  )
}
