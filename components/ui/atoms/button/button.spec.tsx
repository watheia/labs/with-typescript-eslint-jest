import React from "react"
import { render } from "@testing-library/react"
import { expect } from "chai"

import { Button } from "."

describe("button", () => {
  it("should render the contents", () => {
    const { getByText } = render(<Button>click me</Button>)
    const rendered = getByText("click me")

    expect(rendered).to.exist
  })
})
