import React, { useState } from "react"

import { schema, uischema, initialData } from "@waweb/dimensions.model.person"
import { materialRenderers, materialCells } from "@jsonforms/material-renderers"


import { JsonForms } from "@jsonforms/react"

export const Form = (props: Record<string, unknown>) => {
  const [data, setData] = useState(initialData)
  return (
    <div {...props}>
      <JsonForms
        schema={schema}
        uischema={uischema}
        data={data}
        renderers={materialRenderers}
        cells={materialCells}
        onChange={({ data, errors }) => setData(data || errors)}
      />
    </div>
  )
}

export default Form
