import React from "react"
import { TopLayout } from "@waweb/dimensions.ui.layout.top-layout"
import { Container, makeStyles } from "@material-ui/core"
import Form from "."

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(2),
  },
}))

export const Example = () => {
  const classes = useStyles()
  return (
    <TopLayout>
      <Container className={classes.container}>
        <Form />
      </Container>
    </TopLayout>
  )
}
