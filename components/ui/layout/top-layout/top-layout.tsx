import React, { ReactNode } from "react"
import "./sass/top-layout.scss"
import { ThemeProvider, createMuiTheme } from "@material-ui/core"

/**
 * Customize form so each control has more space
 */
const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: "#215C94",
    },
  },
  overrides: {
    MuiFormControl: {
      root: {
        margin: "0.8em 0",
      },
    },
  },
})

export type LayoutProps = {
  children: ReactNode | ReactNode[] | null
}

export const TopLayout = ({ children }: LayoutProps) => {
  return (
    <>
      <div id="wrapper">
        <div id="main">
          <ThemeProvider theme={theme}>{children}</ThemeProvider>
        </div>
        <footer id="footer">
          <p className="copyright">
            &copy; 2021 <a href="#">Watheia Labs, LLC</a>.
          </p>
        </footer>
      </div>
      <div id="bg" />
    </>
  )
}
